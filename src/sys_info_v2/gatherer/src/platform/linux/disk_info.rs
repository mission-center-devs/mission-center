/* sys_info_v2/gatherer/src/platform/linux/disk_info.rs
 *
 * Copyright 2024 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use super::{INITIAL_REFRESH_TS, MIN_DELTA_REFRESH};
use crate::logging::{critical, warning};
use crate::platform::disk_info::{DiskInfoExt, DiskType, DisksInfoExt};
use crate::platform::DiskSmartInterface;
use crate::MK_TO_0_C;
use glob::glob;
use pollster::FutureExt;
use std::cmp::max;
use std::collections::HashMap;
use std::{sync::Arc, time::Instant};
use udisks2::drive::RotationRate;
use udisks2::Client;

#[derive(Debug, Clone, PartialEq)]
pub struct LinuxDiskInfo {
    pub id: Arc<str>,
    pub model: Arc<str>,
    pub r#type: DiskType,
    pub smart_interface: DiskSmartInterface,
    pub capacity: u64,
    pub formatted: u64,
    pub system_disk: bool,

    pub busy_percent: f32,
    pub response_time_ms: f32,
    pub read_speed: u64,
    pub total_read: u64,
    pub write_speed: u64,
    pub total_write: u64,
    pub ejectable: bool,
    pub drive_temperature_k: u32,
}

impl Default for LinuxDiskInfo {
    fn default() -> Self {
        Self {
            id: Arc::from(""),
            model: Arc::from(""),
            r#type: DiskType::default(),
            smart_interface: Default::default(),
            capacity: 0,
            formatted: 0,
            system_disk: false,

            busy_percent: 0.,
            response_time_ms: 0.,
            read_speed: 0,
            total_read: 0,
            write_speed: 0,
            total_write: 0,
            ejectable: false,

            drive_temperature_k: 0,
        }
    }
}

pub struct LinuxDiskInfoIter<'a>(
    pub  std::iter::Map<
        std::slice::Iter<'a, (DiskStats, LinuxDiskInfo)>,
        fn(&'a (DiskStats, LinuxDiskInfo)) -> &'a LinuxDiskInfo,
    >,
);

impl<'a> LinuxDiskInfoIter<'a> {
    pub fn new(iter: std::slice::Iter<'a, (DiskStats, LinuxDiskInfo)>) -> Self {
        Self(iter.map(|(_, di)| di))
    }
}

impl<'a> Iterator for LinuxDiskInfoIter<'a> {
    type Item = &'a LinuxDiskInfo;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl DiskInfoExt for LinuxDiskInfo {
    fn id(&self) -> &str {
        &self.id
    }

    fn model(&self) -> &str {
        &self.model
    }

    fn r#type(&self) -> DiskType {
        self.r#type
    }

    fn smart_interface(&self) -> DiskSmartInterface {
        self.smart_interface
    }

    fn capacity(&self) -> u64 {
        self.capacity
    }

    fn formatted(&self) -> u64 {
        self.formatted
    }

    fn is_system_disk(&self) -> bool {
        self.system_disk
    }

    fn busy_percent(&self) -> f32 {
        self.busy_percent
    }

    fn response_time_ms(&self) -> f32 {
        self.response_time_ms
    }

    fn read_speed(&self) -> u64 {
        self.read_speed
    }

    fn total_read(&self) -> u64 {
        self.total_read
    }

    fn write_speed(&self) -> u64 {
        self.write_speed
    }

    fn total_write(&self) -> u64 {
        self.total_write
    }

    fn ejectable(&self) -> bool {
        self.ejectable
    }

    fn drive_temperature(&self) -> u32 {
        self.drive_temperature_k
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct DiskStats {
    sectors_read: u64,
    sectors_written: u64,

    read_ios: u64,
    write_ios: u64,
    discard_ios: u64,
    flush_ios: u64,
    io_total_time_ms: u64,

    read_ticks_weighted_ms: u64,
    write_ticks_weighted_ms: u64,
    discard_ticks_weighted_ms: u64,
    flush_ticks_weighted_ms: u64,

    read_time_ms: Instant,
}

pub struct LinuxDisksInfo {
    pub client: Result<Client, udisks2::Error>,
    info: Vec<(DiskStats, LinuxDiskInfo)>,

    refresh_timestamp: Instant,
}

impl<'a> DisksInfoExt<'a> for LinuxDisksInfo {
    type S = LinuxDiskInfo;
    type Iter = LinuxDiskInfoIter<'a>;

    fn refresh_cache(&mut self) {
        use crate::{critical, warning};

        let now = Instant::now();
        if now.duration_since(self.refresh_timestamp) < MIN_DELTA_REFRESH {
            return;
        }
        self.refresh_timestamp = now;

        let mut prev_disks = std::mem::take(&mut self.info);

        let client = match &self.client {
            Ok(client) => client,
            Err(e) => {
                critical!(
                    "Gatherer::DiskInfo",
                    "Could not get udisks dbus client: {}",
                    e
                );
                return;
            }
        };

        let objects = match client.object_manager().get_managed_objects().block_on() {
            Ok(objects) => objects,
            Err(e) => {
                critical!(
                    "Gatherer::DiskInfo",
                    "Could not get udisks dbus client: {}",
                    e
                );
                return;
            }
        };

        let mut block_list = vec![];
        let mut drive_block_map = HashMap::new();
        let mut drive_path_map = HashMap::new();

        for (object_path, _) in objects {
            let mut object = match client.object(object_path.clone()) {
                Ok(o) => o,
                Err(_) => continue,
            };
            if let Ok(drive) = object.drive().block_on() {
                drive_block_map.insert(object_path, (drive, object, vec![]));
            } else {
                let partition = object.partition().block_on();

                let drive = if let Ok(block) = object.block().block_on() {
                    let Ok(drive_path) = block.drive().block_on() else {
                        continue;
                    };
                    drive_path_map.insert(
                        drive_path.clone(),
                        String::from_utf8(block.device().block_on().unwrap()).unwrap(),
                    );
                    let drive = drive_path.to_string();
                    if drive == "/" {
                        continue;
                    }
                    drive_path
                } else {
                    continue;
                };

                if let Ok(encrypted) = object.encrypted().block_on() {
                    let Ok(new_object_path) = encrypted.cleartext_device().block_on() else {
                        // TODO: how tf did i get here?
                        continue;
                    };

                    object = match client.object(new_object_path.clone()) {
                        Ok(o) => o,
                        Err(_) => {
                            continue;
                        }
                    };
                }

                if let Ok(block) = object.block().block_on() {
                    let fs = object.filesystem().block_on();

                    block_list.push((block, partition, fs, drive));
                }
            }
        }

        for (block, partition, filesystem, parent) in block_list {
            if let Some((_, _, ref mut block_list)) = drive_block_map.get_mut(&parent) {
                block_list.push((block, partition, filesystem));
            }
        }

        for (object_path, (drive, object, blocks)) in drive_block_map.iter() {
            let drive_ata = object.drive_ata().block_on();

            let dirs = std::fs::read_dir("/sys/block").unwrap();

            // leaving this here just in case...
            /*            let Ok(raw_dir_name) = block
                .device()
                .block_on()
                .map(|it| String::from_utf8(it).unwrap())
            else {
                continue;
            };*/

            let Some(raw_dir_name) = drive_path_map.get(object_path) else {
                // should never happen
                critical!(
                    "Gatherer::DiskInfo",
                    "Could not find drive entry: {}",
                    object_path
                );
                continue;
            };

            let Some(raw_dir_name) = std::path::Path::new(raw_dir_name)
                .file_name()
                .map(|it| it.to_str().unwrap().trim_matches(char::from(0)))
            else {
                continue;
            };

            let mut dir_name = None;

            for dir in dirs.filter_map(Result::ok) {
                if raw_dir_name.starts_with(dir.file_name().to_str().unwrap()) {
                    dir_name = Some(dir.file_name().into_string().unwrap());
                    break;
                }
            }

            let Some(dir_name) = dir_name else {
                critical!(
                    "MissionCenter::DiskInfo",
                    "Failed to find device name for: {:?}",
                    raw_dir_name
                );
                continue;
            };

            let mut prev_disk_index = None;
            for i in 0..prev_disks.len() {
                if prev_disks[i].1.id.as_ref() == dir_name {
                    prev_disk_index = Some(i);
                    break;
                }
            }

            let stats = std::fs::read_to_string(format!("/sys/block/{}/stat", dir_name));

            let stats = match stats.as_ref() {
                Err(e) => {
                    warning!(
                        "MissionCenter::DiskInfo",
                        "Failed to read disk stat: {:?}",
                        e
                    );
                    ""
                }
                Ok(stats) => stats.trim(),
            };

            let mut read_ios = 0;
            let mut sectors_read = 0;
            let mut read_ticks_weighted_ms = 0;
            let mut write_ios = 0;
            let mut sectors_written = 0;
            let mut write_ticks_weighted_ms = 0;
            let mut io_total_time_ms: u64 = 0;
            let mut discard_ios = 0;
            let mut discard_ticks_weighted_ms = 0;
            let mut flush_ios = 0;
            let mut flush_ticks_weighted_ms = 0;

            const IDX_READ_IOS: usize = 0;
            const IDX_READ_SECTORS: usize = 2;
            const IDX_READ_TICKS: usize = 3;
            const IDX_WRITE_IOS: usize = 4;
            const IDX_WRITE_SECTORS: usize = 6;
            const IDX_WRITE_TICKS: usize = 7;
            const IDX_IO_TICKS: usize = 9;
            const IDX_DISCARD_IOS: usize = 11;
            const IDX_DISCARD_TICKS: usize = 14;
            const IDX_FLUSH_IOS: usize = 15;
            const IDX_FLUSH_TICKS: usize = 16;
            for (i, entry) in stats
                .split_whitespace()
                .enumerate()
                .map(|(i, v)| (i, v.trim()))
            {
                match i {
                    IDX_READ_IOS => read_ios = entry.parse::<u64>().unwrap_or(0),
                    IDX_READ_SECTORS => sectors_read = entry.parse::<u64>().unwrap_or(0),
                    IDX_READ_TICKS => read_ticks_weighted_ms = entry.parse::<u64>().unwrap_or(0),
                    IDX_WRITE_IOS => write_ios = entry.parse::<u64>().unwrap_or(0),
                    IDX_WRITE_SECTORS => sectors_written = entry.parse::<u64>().unwrap_or(0),
                    IDX_WRITE_TICKS => write_ticks_weighted_ms = entry.parse::<u64>().unwrap_or(0),
                    IDX_IO_TICKS => {
                        io_total_time_ms = entry.parse::<u64>().unwrap_or(0);
                    }
                    IDX_DISCARD_IOS => discard_ios = entry.parse::<u64>().unwrap_or(0),
                    IDX_DISCARD_TICKS => {
                        discard_ticks_weighted_ms = entry.parse::<u64>().unwrap_or(0)
                    }
                    IDX_FLUSH_IOS => flush_ios = entry.parse::<u64>().unwrap_or(0),
                    IDX_FLUSH_TICKS => {
                        flush_ticks_weighted_ms = entry.parse::<u64>().unwrap_or(0);
                        break;
                    }
                    _ => (),
                }
            }

            let temp_inputs = glob(
                format!(
                    "/sys/block/{}/device/hwmon[0-9]*/temp[0-9]*_input",
                    dir_name
                )
                .as_str(),
            )
            .unwrap()
            .filter_map(Result::ok)
            .filter_map(|f| std::fs::read_to_string(f).ok())
            .filter_map(|v| v.trim().parse::<i64>().ok())
            .map(|i| (i + MK_TO_0_C as i64) as u32)
            .collect::<Vec<_>>();

            let drive_temperature_k = if !temp_inputs.is_empty() {
                temp_inputs[0]
            } else {
                match &drive_ata {
                    Ok(drive_ata) => drive_ata
                        .smart_temperature()
                        .block_on()
                        .map(|f| (f * 1000.) as u32)
                        .unwrap_or(0),
                    Err(_) => 0,
                }
            };

            // we check out here in case of media removal or similar
            // TODO: should we handle empty drives in the UI?
            let capacity = if let Some((root_block, _, _)) =
                blocks.iter().find(|(_, partition, _)| partition.is_err())
            {
                root_block.size().block_on().unwrap_or(0)
            } else {
                blocks
                    .iter()
                    .filter_map(|(it, _, _)| it.size().block_on().ok())
                    .sum()
            };

            if let Some((mut disk_stat, mut info)) = prev_disk_index.map(|i| prev_disks.remove(i)) {
                let read_ticks_weighted_ms_prev =
                    if read_ticks_weighted_ms < disk_stat.read_ticks_weighted_ms {
                        read_ticks_weighted_ms
                    } else {
                        disk_stat.read_ticks_weighted_ms
                    };

                let write_ticks_weighted_ms_prev =
                    if write_ticks_weighted_ms < disk_stat.write_ticks_weighted_ms {
                        write_ticks_weighted_ms
                    } else {
                        disk_stat.write_ticks_weighted_ms
                    };

                let discard_ticks_weighted_ms_prev =
                    if discard_ticks_weighted_ms < disk_stat.discard_ticks_weighted_ms {
                        discard_ticks_weighted_ms
                    } else {
                        disk_stat.discard_ticks_weighted_ms
                    };

                let flush_ticks_weighted_ms_prev =
                    if flush_ticks_weighted_ms < disk_stat.flush_ticks_weighted_ms {
                        flush_ticks_weighted_ms
                    } else {
                        disk_stat.flush_ticks_weighted_ms
                    };

                let elapsed = disk_stat.read_time_ms.elapsed().as_secs_f32();

                let delta_read_ticks_weighted_ms =
                    read_ticks_weighted_ms - read_ticks_weighted_ms_prev;
                let delta_write_ticks_weighted_ms =
                    write_ticks_weighted_ms - write_ticks_weighted_ms_prev;
                let delta_discard_ticks_weighted_ms =
                    discard_ticks_weighted_ms - discard_ticks_weighted_ms_prev;
                let delta_flush_ticks_weighted_ms =
                    flush_ticks_weighted_ms - flush_ticks_weighted_ms_prev;
                let delta_ticks_weighted_ms = delta_read_ticks_weighted_ms
                    + delta_write_ticks_weighted_ms
                    + delta_discard_ticks_weighted_ms
                    + delta_flush_ticks_weighted_ms;

                // Arbitrary math is arbitrary
                let busy_percent = (delta_ticks_weighted_ms as f32 / (elapsed * 8.0)).min(100.);

                disk_stat.read_ticks_weighted_ms = read_ticks_weighted_ms;
                disk_stat.write_ticks_weighted_ms = write_ticks_weighted_ms;
                disk_stat.discard_ticks_weighted_ms = discard_ticks_weighted_ms;
                disk_stat.flush_ticks_weighted_ms = flush_ticks_weighted_ms;

                let io_time_ms_prev = if io_total_time_ms < disk_stat.io_total_time_ms {
                    io_total_time_ms
                } else {
                    disk_stat.io_total_time_ms
                };

                let read_ios_prev = if read_ios < disk_stat.read_ios {
                    read_ios
                } else {
                    disk_stat.read_ios
                };

                let write_ios_prev = if write_ios < disk_stat.write_ios {
                    write_ios
                } else {
                    disk_stat.write_ios
                };

                let discard_ios_prev = if discard_ios < disk_stat.discard_ios {
                    discard_ios
                } else {
                    disk_stat.discard_ios
                };

                let flush_ios_prev = if flush_ios < disk_stat.flush_ios {
                    flush_ios
                } else {
                    disk_stat.flush_ios
                };

                let delta_io_time_ms = io_total_time_ms - io_time_ms_prev;
                let delta_read_ios = read_ios - read_ios_prev;
                let delta_write_ios = write_ios - write_ios_prev;
                let delta_discard_ios = discard_ios - discard_ios_prev;
                let delta_flush_ios = flush_ios - flush_ios_prev;

                let delta_ios =
                    delta_read_ios + delta_write_ios + delta_discard_ios + delta_flush_ios;
                let response_time_ms = if delta_ios > 0 {
                    delta_io_time_ms as f32 / delta_ios as f32
                } else {
                    0.
                };

                disk_stat.read_ios = read_ios;
                disk_stat.write_ios = write_ios;
                disk_stat.discard_ios = discard_ios;
                disk_stat.flush_ios = flush_ios;
                disk_stat.io_total_time_ms = io_total_time_ms;

                let sectors_read_prev = if sectors_read < disk_stat.sectors_read {
                    sectors_read
                } else {
                    disk_stat.sectors_read
                };

                let sectors_written_prev = if sectors_written < disk_stat.sectors_written {
                    sectors_written
                } else {
                    disk_stat.sectors_written
                };

                let read_speed = ((sectors_read - sectors_read_prev) as f32 * 512.) / elapsed;
                let write_speed =
                    ((sectors_written - sectors_written_prev) as f32 * 512.) / elapsed;

                let read_speed = read_speed.round() as u64;
                let write_speed = write_speed.round() as u64;

                disk_stat.sectors_read = sectors_read;
                disk_stat.sectors_written = sectors_written;

                disk_stat.read_time_ms = Instant::now();

                info.capacity = capacity;

                info.busy_percent = busy_percent;
                info.response_time_ms = response_time_ms;
                info.read_speed = read_speed;
                info.total_read = sectors_read * 512;
                info.write_speed = write_speed;
                info.total_write = sectors_written * 512;

                info.drive_temperature_k = drive_temperature_k;

                self.info.push((disk_stat, info));
            } else {
                let mut formatted = 0;
                let mut has_root = false;

                for (b, p, f) in blocks {
                    let mut thissize = max(
                        p.clone()
                            .map(|it| it.size().block_on().unwrap_or_default())
                            .unwrap_or_default(),
                        f.clone()
                            .map(|it| it.size().block_on().unwrap_or_default())
                            .unwrap_or_default(),
                    );

                    // if this is not a root partition (like nvme0n1, sometimes the partition or filesystem report wrong)
                    if p.is_ok() || f.is_ok() {
                        thissize = max(thissize, b.size().block_on().unwrap_or_default());
                    }

                    formatted += thissize;

                    if let Ok(f) = f {
                        if let Ok(mountpoints) = f.mount_points().block_on() {
                            let mountpoints = mountpoints
                                .iter()
                                .map(|p| String::from_utf8(p.clone()).unwrap_or("".to_string()));

                            has_root |= mountpoints
                                .map(|it| it.trim_matches(char::from(0)) == "/")
                                .reduce(|out, curr| out || curr)
                                .unwrap_or(false);
                        } else {
                            critical!(
                                "Gatherer::DiskInfo",
                                "Failed to read partitions for: {}",
                                dir_name
                            );
                        }
                    }
                }

                let result = object.nvme_controller().block_on();
                let smart_interface = if drive_ata.is_ok() {
                    DiskSmartInterface::Ata
                } else if result.is_ok() {
                    DiskSmartInterface::NVMe
                } else {
                    DiskSmartInterface::Dumb
                };

                let r#type = if dir_name.starts_with("nvme") {
                    DiskType::NVMe
                } else if dir_name.starts_with("mmc") {
                    Self::get_mmc_type(&dir_name)
                } else if dir_name.starts_with("fd") {
                    DiskType::Floppy
                } else if dir_name.starts_with("sr") {
                    // TODO: specify what type better
                    DiskType::Optical
                } else {
                    match drive.rotation_rate().block_on() {
                        Ok(RotationRate::NonRotating) | Ok(RotationRate::Rotating(0)) => {
                            if drive.removable().block_on().unwrap_or(false) {
                                // FIXME This was `Flash`, do we want that or something else?
                                DiskType::SSD
                            } else {
                                DiskType::SSD
                            }
                        }
                        Ok(RotationRate::Rotating(_)) => DiskType::HDD,
                        _ => DiskType::Unknown,
                    }
                };
                // TODO: should we do this?
                /*                let r#type = if drive.optical().block_on().unwrap_or(false) {
                                    DiskType::Optical
                                } else {
                                    let rate = drive.rotation_rate().block_on().unwrap_or(Unknown);
                                    if rate == NonRotating || rate == Unknown {
                                        if dir_name.starts_with("nvme") {
                                            DiskType::NVMe
                                        } else if dir_name.starts_with("mmc") {
                                            Self::get_mmc_type(&dir_name)
                                        } else {
                                            DiskType::SSD
                                        }
                                    } else {
                                        // it is rotating and not optical, as per above
                                        DiskType::HDD
                                    }
                                };
                */
                let vendor = drive.vendor().block_on().unwrap_or("".to_string());

                let model = drive.model().block_on().unwrap_or("".to_string());

                let model = Arc::<str>::from(format!("{} {}", vendor.trim(), model.trim()).trim());

                self.info.push((
                    DiskStats {
                        sectors_read,
                        sectors_written,
                        read_ios,
                        write_ios,
                        discard_ios,
                        flush_ios,
                        io_total_time_ms,
                        read_ticks_weighted_ms,
                        write_ticks_weighted_ms,
                        discard_ticks_weighted_ms,
                        flush_ticks_weighted_ms,
                        read_time_ms: Instant::now(),
                    },
                    LinuxDiskInfo {
                        id: Arc::from(dir_name),
                        model,
                        r#type,
                        smart_interface,
                        capacity,
                        formatted,
                        system_disk: has_root,

                        busy_percent: 0.,
                        response_time_ms: 0.,
                        read_speed: 0,
                        total_read: 0,
                        write_speed: 0,
                        total_write: 0,
                        ejectable: drive.ejectable().block_on().unwrap_or(false),
                        drive_temperature_k,
                    },
                ));
            }
        }
    }

    fn info(&'a self) -> Self::Iter {
        LinuxDiskInfoIter::new(self.info.iter())
    }
}

impl LinuxDisksInfo {
    pub fn new() -> Self {
        Self {
            client: Client::new().block_on(),
            info: vec![],

            refresh_timestamp: *INITIAL_REFRESH_TS,
        }
    }

    fn get_mmc_type(dir_name: &String) -> DiskType {
        let Some(hwmon_idx) = dir_name[6..].parse::<u64>().ok() else {
            return DiskType::Unknown;
        };

        let globs = match glob(&format!(
            "/sys/class/mmc_host/mmc{}/mmc{}*/type",
            hwmon_idx, hwmon_idx
        )) {
            Ok(globs) => globs,
            Err(e) => {
                warning!("Gatherer::DiskInfo", "Failed to read mmc type entry: {}", e);
                return DiskType::Unknown;
            }
        };

        let mut res = DiskType::Unknown;
        for entry in globs {
            res = match entry {
                Ok(path) => match std::fs::read_to_string(&path).ok() {
                    Some(typ) => match typ.trim() {
                        "SD" => DiskType::SD,
                        "MMC" => DiskType::eMMC,
                        _ => {
                            critical!("Gatherer::DiskInfo", "Unknown mmc type: '{}'", typ);
                            continue;
                        }
                    },
                    _ => {
                        critical!(
                            "Gatherer::DiskInfo",
                            "Could not read mmc type: {}",
                            path.display()
                        );
                        continue;
                    }
                },
                _ => {
                    continue;
                }
            };
        }
        res
    }
}
